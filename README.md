# Vizualizator tokov v sieti

## Instalacia (OS Linux)
1. Nainstalovat JDK, idealne oracle-jdk-8. Nastavit cestu ku JDK do JAVA_HOME.

2. Stahnut Typesafe Activator: http://downloads.typesafe.com/typesafe-activator/1.3.2/typesafe-activator-1.3.2.zip
a rozbalit do lubovolnej zlozky ${ACTIVATOR}.

3. ${ACTOVATOR}/bin zaradit do PATH
``` 
PATH="${ACTOVATOR}/bin:$PATH"
```

4. Nainstalovat zo standardneho repozitara `libpcap`.

5. Stiahnut [GeoLite2-City.mmdb](http://dev.maxmind.com/geoip/geoip2/geolite2/).

## Konfiguracia

V zlozke `fwizweb/conf/application.conf ` sa nachadza konfiguracny subor aplikacie. Dolezite su nasledujuce tri nastavenia
```
db.default.url="jdbc:h2:~/flows;DB_CLOSE_DELAY=-1"        # H2 databaza ulozi data do suboru ~/flows.db .
# ...
fwizweb.mmdb="/home/lukas/GeoLite2-City.mmdb"             # cesta ku GeoLite2 databaze.
fwizweb.default.pcap="/home/lukas/tracefile-100MB"        # Cesta ku vstupnemu suboru s netflow zaznamamy.

```

## Spustenie aplikacie

``` 
cd ${PROJECT_DIR}
activator run
```

Prve spustenie moze trvat niekolko minut, nakolko activator stahuje Play framework a vsetky zavisloisti projektu.

Zaroven prebehne inicialne nacitanie tokov, ktore takisto moze trvat nejaky cas v zavislosti od velkosti vstupneho suboru.

Aplikacie po spusteni bude mozne prehliadat na adrese http://localhost:9000 .


