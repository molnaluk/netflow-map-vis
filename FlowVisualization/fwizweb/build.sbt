name := "fwizweb"

version := "1.0"

lazy val `fwizweb` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

resolvers += "Clojars" at "http://clojars.org/repo/"

libraryDependencies ++= Seq(jdbc, anorm, cache, ws)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

// libraryDependencies += "jnetpcap" % "jnetpcap" % "1.4.r1425-1f"

libraryDependencies += "com.sanoma.cda" %% "maxmind-geoip2-scala" % "1.3.5"

libraryDependencies += "org.pcap4j" % "pcap4j-core" % "1.4.0"

libraryDependencies += "jp.t2v" %% "play2-auth" % "0.13.2"

libraryDependencies += "jp.t2v" %% "play2-auth-test" % "0.13.2" % "test"
