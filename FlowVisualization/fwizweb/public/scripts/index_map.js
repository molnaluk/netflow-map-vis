/*
 Element.prototype.hide = function () {
 this.node.style.display = "none";
 //this.node.style.visibility = "hidden";
 return this;
 };
 Element.prototype.show = function () {
 this.node.style.display = "block";
 //this.node.style.visibility = "visible";
 return this;
 };
 Element.prototype.IsVisible = function () {
 return this.node.style.display == "block";
 }
 Element.prototype.remove = function () {
 this.node.parentNode.removeChild(this.node);
 };
 Element.prototype.getBBox = function () {
 var hide = false;
 if (!this.IsVisible()) {
 this.show();
 this.node.style.display = "block";
 hide = true;
 }
 var bbox = {};
 if (this.type == "text") {
 bbox = {x: bbox.x, y: Infinity, width: bbox.width, height: 0};
 for (var i = 0, ii = this.node.getNumberOfChars(); i < ii; i++) {
 var bb = this.node.getExtentOfChar(i);
 (bb.y < bbox.y) && (bbox.y = bb.y);
 (bb.y + bb.height - bbox.y > bbox.height) && (bbox.height = bb.y + bb.height - bbox.y);
 }
 } else {
 bbox = this.node.getBBox()
 }
 if (hide) {this.hide(); this.node.style.display = "none"}
 return bbox;
 };
 */

var Index = function () {
    var $win = $(window);
    var self = {
        //main function
        setWidth: function (map) {
            var width = $win.width();
            var height = $win.height() - $('#btn_toolbar').height();
            if (height > 0 == false) height = $win.height();
            if (width > 0 && width != Number.POSITIVE_INFINITY) {
                map.parent().width(width).height(height);
                map.width(width).height(height);
                map.find('svg').width(width).height(height);
            }
        },
        init: function () {
            Metronic.addResizeHandler(function () {
                jQuery('.vmapsqqq').each(function () {
                    self.setWidth(jQuery(this));
                });
            });
        },

        resize: function () {
            //var height = $win.height() - $('#btn_toolbar').height();
            //if (height && height > 0 && height < Number.POSITIVE_INFINITY) {
            //    $('.jvectormap-container').height(height).width(height);
            //}
        },

        initVectorMap: function () {
            self.setVectorMap = function (name, valueKeys) {
                var map = jQuery('#vmap_' + name);
                if (!map) {
                    return;
                }
                map.vectorMap({
                    map: name + '_en',
                    markers: self.data.coords,
                    series: {
                        markers: [{
                            attribute: 'fill',
                            scale: ['#FEE5D9', '#A50F15'],
                            normalizeFunction: 'polynomial',
                            values: self.data.values.cities[valueKeys.city1]
                            //min: jvm.min(self.data.values.cities[valueKeys.city1]),
                            //max: jvm.max(self.data.values.cities[valueKeys.city1])
                        }, {
                            attribute: 'r',
                            scale: [6, 30],
                            values: self.data.values.cities[valueKeys.city2]
                        }],
                        regions: [{
                            scale: ['#b6da93', '#b0d090', '#a3ea71', '#9aba31', '#909cae'], //['#DEEBF7', '#08519C'],
                            attribute: 'fill',
                            normalizeFunction: 'polynomial',
                            values: self.data.countries
                        }]
                    },
                    onMarkerTipShow: function (event, label, index) {
                        label.html(
                            '<b>' + self.data.city_names[index] + '</b><br/>' +
                            '<b>' + valueKeys.city1 + ': </b>' + self.data.values.cities[valueKeys.city1][index] + '</br>' +
                            '<b>' + valueKeys.city2 + ': </b>' + self.data.values.cities[valueKeys.city2][index]
                        );
                    },
                    onRegionTipShow: function (event, label, code) {
                        label.html(
                            '<b>' + label.html() + '</b></br>' +
                            '<b> ' + valueKeys.country + ': </b>' + self.data.countries[code]
                        );
                    }
                });

            };

            self.showMap = function (name) {
                jQuery('.vmapsqqq').hide();
                //self.resize();
                self.setWidth(jQuery('#vmap_' + name).show().resize());
            };
            jQuery.getJSON('/api/flows/globe2' + window.location.search, function (data) {
                console.log(data);
                self.data = data;
                jQuery.each(["world_mill", "europe_mill" /*, "north-america", "south-america", "africa", "asia" , "australia", "russia", "germany", "usa" */],
                    function (i, val) {
                        self.setVectorMap(val, {city1: 'data_size_avg', city2: 'count', country: 'count'});
                    });
                self.showMap("world_mill");
            });

            jQuery(".regional_stat").click(function () {
                self.showMap($(this).data('map'));
            });

            $('#region_statistics_loading').hide();
        }
    };
    $win.resize(self.resize);
    self.resize();
    return self;
}();
