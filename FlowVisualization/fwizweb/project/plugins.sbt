logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Clojars" at "http://clojars.org/repo/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.3.8")