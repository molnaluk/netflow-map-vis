import model.Role._
import model._
import parsers.Pcap2NF
import play.api.Play.current
import play.api._

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    if (Account.findAll.isEmpty) {
      Logger.info("Creating accounts")
      Seq(
        Account(1, "molnaluk@fit.cvut.cz", "321vstuP", "Lukas", Admin),
        Account(2, "visitor@visitor", "seKret", "Bob", NormalUser)
      ) foreach Account.create
    }

    if (GeoDataResolver.geoTable.isEmpty) {
      val geodata = GeoData.findAll
      Logger.info(s"GeoData count: ${geodata.length}")
      geodata.foreach(g => {
        GeoDataResolver.geoTable.update(g.id, g)
        GeoDataResolver.geoTableByIP.update(g.key, g)
      })
      GeoDataResolver.reloadIPtable
    }
    val count = Flows.count
    Logger.info(s"Flow count: ${count}")
    if (count == 0) {

      val capture = Util.time {
        Flows.load(None, new Pcap2NF(_ => true, current.configuration.getInt("fwizweb.limit")))
      }
      Logger.info(capture.toString)
      Logger.info(s"Flow count: ${Flows.count}")
    }
  }
}

object Util {

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block // call-by-name
    val t1 = System.nanoTime()
    Logger.info(s"Elapsed time: ${(t1 - t0) / 1000000}ms")
    result
  }
}
