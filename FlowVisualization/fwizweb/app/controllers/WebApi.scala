package controllers

import com.sanoma.cda.geo.Point
import jp.t2v.lab.play2.auth.AuthElement
import model.Role._
import model._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{Action, Controller}

/**
 * Created by lukas on 1.5.15.
 */
object WebApi extends Controller with AuthElement with AuthConfigImpl {

  def flow(id: Long) = StackAction(AuthorityKey -> Admin) { implicit request =>
    //val pcapFile = current.configuration.getString("fwizweb.default.pcap").get
    Ok(Json.toJson(Flows.find(id)))
  }

  def flows() = StackAction(AuthorityKey -> Admin) { implicit request =>
    Ok(Json.toJson(Flows.all()))
  }

  def flows_globe(start: Option[Long], end: Option[Long], port: Option[Int], protocol: Option[Int], ipv6: Option[Boolean]) = Action {
    //StackAction(AuthorityKey -> Admin) { implicit request =>
    val proto = if (protocol.isDefined) Some[Short](protocol.get.toShort) else None
    val countries = AggregatedFlows.find("c.code", "COUNTRY.CODE", FlowFilter(start, end, port, proto, ipv6))
    val continents = AggregatedFlows.find("c.continent", "COUNTRY.CONTINENT", FlowFilter(start, end, port, proto, ipv6))
    Ok(Json.toJson(GlobeViewModel(countries, None, Some(continents))))
  }

  def flows_globe2(start: Option[Long], end: Option[Long], port: Option[Int], protocol: Option[Int], ipv6: Option[Boolean]) = Action {
    //StackAction(AuthorityKey -> Admin) { implicit request =>
    val proto = if (protocol.isDefined) Some[Short](protocol.get.toShort) else None
    val countries = AggregatedFlows.find("c.code", "COUNTRY.CODE", FlowFilter(start, end, port, proto, ipv6))
    val cities = AggregatedFlows.find("city, gdid", "GEODATA.CITY", FlowFilter(start, end, port, proto, ipv6), true)
    val continents = AggregatedFlows.find("c.continent", "COUNTRY.CONTINENT", FlowFilter(start, end, port, proto, ipv6))
    val values = Seq("continents" -> AggregatedFlows.turbo(continents),
      "cities" -> AggregatedFlows.turbo(cities)).toMap
    val countryCode = (li: Seq[AggregatedFlow]) => if (li(0).place.isDefined) li(0).place.get else "??"
    val countryMap = countries.map(c => countryCode(c) -> BigDecimal(c(0).count)).toMap
    Ok(Json.toJson(GlobeViewModel2(values,
      AggregatedFlows.coord(cities),
      cities.map(c => if (c(0).geo.isDefined) c(0).geo.get.key.city else Some("?")),
      // countries.map( c => if (c(0).place.isDefined) c(0).place.get else "").zipWithIndex.toMap
      countryMap
    )))
  }

  def geo() = Action {
    Ok("No data")
  }

  def countries() = Action {
    Ok(Json.toJson(Country.getAll()))
  }

  def flowsByCountry(start: Option[Long], end: Option[Long]) = Action {
    Ok(Json.toJson(Flows.country(FlowFilter(start, end))))
  }

  def flowsByContinets(start: Option[Long], end: Option[Long]) = Action {
    Ok(Json.toJson(Flows.country(FlowFilter(start, end))))
  }

  /*  implicit val pointWrites: Writes[Point] = (

      (JsPath \ "lat").write[Double] and
        (JsPath \ "lon").write[Double]
      )(unlift(Point.unapply))*/

  implicit val arrPoint = new Writes[Point] {
    def writes(p: Point) = JsArray(Seq[JsValue](JsNumber(p.latitude), JsNumber(p.longitude)))
  }

  implicit val geoKeyWrites: Writes[GeoDataKey] = (
    (JsPath \ "country_id").write[Int] and
      (JsPath \ "city").write[Option[String]] and
      (JsPath \ "ip_dst").write[Option[Point]]
    )(unlift(GeoDataKey.unapply))

  implicit val geoWrites: Writes[GeoData] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "key").write[GeoDataKey]
    )(unlift(GeoData.unapply))

  implicit val countryWrites: Writes[Country] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "code").write[String] and
      (JsPath \ "name").write[String] and
      (JsPath \ "continent").write[String]
    )(unlift(Country.unapply))

  implicit val flowWrites: Writes[Flow] = (
    (JsPath \ "capture_id").write[Int] and
      (JsPath \ "ip_src").write[String] and
      (JsPath \ "ip_dst").write[String] and
      (JsPath \ "port_src").write[Int] and
      (JsPath \ "port_dst").write[Int] and
      (JsPath \ "start_time").write[Long] and
      (JsPath \ "end_time").write[Long] and
      (JsPath \ "packet_count").write[Long] and
      (JsPath \ "data_size").write[Long] and
      (JsPath \ "proto").write[Int] and
      (JsPath \ "src_geo").write[Option[GeoData]] and
      (JsPath \ "dst_geo").write[Option[GeoData]] and
      (JsPath \ "ipv6").write[Boolean]
    )(unlift(Flow.unapply))

  implicit val aggregWites: Writes[AggregatedFlow] = (
    (JsPath \ "count").write[Long] and
      (JsPath \ "packet_count").write[BigDecimal] and
      (JsPath \ "data_size").write[BigDecimal] and
      (JsPath \ "duration").write[BigDecimal] and
      (JsPath \ "port").write[Option[Int]] and
      (JsPath \ "ip").write[Option[String]] and
      (JsPath \ "place").write[Option[String]] and
      (JsPath \ "geo").write[Option[GeoData]]
    )(unlift(AggregatedFlow.unapply))

  implicit val globeWrites: Writes[GlobeViewModel] = (
    (JsPath \ "countries").write[List[Seq[AggregatedFlow]]] and
      (JsPath \ "cities").write[Option[List[Seq[AggregatedFlow]]]] and
      (JsPath \ "continents").write[Option[List[Seq[AggregatedFlow]]]]
    )(unlift(GlobeViewModel.unapply))

  implicit val globe2Writes: Writes[GlobeViewModel2] = (
    (JsPath \ "values").write[Map[String, Map[String, List[BigDecimal]]]] and
      (JsPath \ "coords").write[List[Option[Point]]] and
      (JsPath \ "city_names").write[List[Option[String]]] and
      (JsPath \ "countries").write[Map[String, BigDecimal]]
    )(unlift(GlobeViewModel2.unapply))
}

