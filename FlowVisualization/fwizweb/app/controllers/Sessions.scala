package controllers

import jp.t2v.lab.play2.auth.LoginLogout
import model.Account
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._

import scala.concurrent.Future

object Sessions extends Controller with LoginLogout with AuthConfigImpl {


  /** Your application's login form.  Alter it to fit your application */
  val loginForm = Form {
    mapping("email" -> email, "password" -> text)(Account.authenticate)(_.map(u => (u.email, "")))
      .verifying("Invalid email or password", result => result.isDefined)
  }

  val remembermeForm = Form {
    "rememberme" -> boolean
  }

  /** Alter the login page action to suit your application. */
  def login = Action { implicit request =>
    Ok(views.html.login(loginForm, remembermeForm.fill(request.session.get("rememberme").exists("true" ==))))
  }

  /**
   * Return the `gotoLogoutSucceeded` method's result in the logout action.
   *
   * Since the `gotoLogoutSucceeded` returns `Future[Result]`,
   * you can add a procedure like the following.
   *
   * gotoLogoutSucceeded.map(_.flashing(
   * "success" -> "You've been logged out"
   * ))
   */
  def logout = Action.async { implicit request =>
    gotoLogoutSucceeded.map(_.flashing(
      "success" -> "You've been logged out"
    ) // .removingFromSession("rememberme")
    )
  }

  /**
   * Return the `gotoLoginSucceeded` method's result in the login action.
   *
   * Since the `gotoLoginSucceeded` returns `Future[Result]`,
   * you can add a procedure like the `gotoLogoutSucceeded`.
   */
  def authenticate = Action.async { implicit request =>
    val rememberme = remembermeForm.bindFromRequest()
    loginForm.bindFromRequest.fold(
    formWithErrors => Future.successful(BadRequest(views.html.login(formWithErrors, rememberme))), { user =>
      val req = request.copy(tags = request.tags + ("rememberme" -> rememberme.get.toString))
      gotoLoginSucceeded(user.get.id)(req, defaultContext).map(_.withSession("rememberme" -> rememberme.get.toString))
    }
    )
  }

}
