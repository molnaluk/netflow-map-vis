package controllers

import jp.t2v.lab.play2.auth.AuthElement
import model.Role._
import model._
import parsers.{FlowCapturer, FlowItem, Pcap2NF}
import play.api.Play._
import play.api.mvc._

import scala.collection.mutable

object Application extends Controller with AuthElement with AuthConfigImpl {

  def index = Action {
    // StackAction(AuthorityKey -> NormalUser) { implicit request =>
    Ok(views.html.globe()) //views.html.index("Your new application is ready.")
  }

  def globe = Action {
    // StackAction(AuthorityKey -> NormalUser) { implicit request =>
    Ok(views.html.globe())
  }

  def pcap(path: Option[String]) = StackAction(AuthorityKey -> Admin) { implicit request =>
    val pcapFile = if (path.isDefined) path.get else current.configuration.getString("fwizweb.default.pcap").get
    val nf = new Pcap2NF(fi => fi.inputInterface == 201 || fi.inputInterface == 206 ||
      fi.outputInterface == 201 || fi.outputInterface == 206,
      Some(64))
    val flowsNF = mutable.MutableList[FlowItem]()
    nf.readFile(pcapFile, (f, _) => {
      flowsNF ++= f; true
    })
    Ok(views.html.pcap(flowsNF))
  }

  def loadpcap(path: Option[String]) = StackAction(AuthorityKey -> Admin) { implicit request =>
    val flows = mutable.MutableList[Flow]()
    val capturer: FlowCapturer = new Pcap2NF(fi => fi.inputInterface == 201 || fi.inputInterface == 206 ||
      fi.outputInterface == 201 || fi.outputInterface == 206, Some(64))
    val capt = Flows.load(path, capturer)
    Ok(views.html.flowtable(flows))
  }

  def flows() = Action {
    //StackAction(AuthorityKey -> NormalUser) { implicit request =>
    Ok(views.html.flowtable(Flows.all()))
  }

}
