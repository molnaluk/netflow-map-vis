package model

/**
 * Created by lukas on 26.4.15.
 */
object Continent extends Enumeration {
  type Continent = Value

  val Unknown, Europe, Africa, Asia, Australia, Antarctica, NorthAmerica, SouthAmerica = Value
}
