package model

/**
 * Created by lukas on 4.5.15.
 */
case class FlowFilter(start_time: Option[Long] = None,
                      end_time: Option[Long] = None,
                      port: Option[Int] = None,
                      protocol: Option[Short] = None,
                      ipv6: Option[Boolean] = None,
                      capture: List[Int] = List()) {

  def join(isSrc: Boolean = true) = {
    val prefix = if (isSrc) "src" else "dst"
    s"""INNER JOIN GeoData ${prefix} ON f.${prefix}_geo = ${prefix}.gdid
INNER JOIN Country c ON c.id = ${prefix}.country_id"""
  }

  def where() = {
    val sb = new StringBuilder(" WHERE ")
    if (start_time.isDefined) {
      sb.append("f.start_time >=  ").append(start_time.get).append(' ')
    }
    if (end_time.isDefined) {
      if (sb.length > 7) sb.append("AND ")
      sb.append("f.end_time <= ").append(end_time.get).append(' ')
    }

    if (port.isDefined) {
      if (sb.length > 7) sb.append("AND ")
      sb.append("f.port_src = ").append(port.get).append(' ').append(" OR port_dst = ").append(port.get).append(' ')
    }

    if (ipv6.isDefined) {
      if (sb.length > 7) sb.append("AND ")
      sb.append("f.ipv6 = ").append(ipv6.get.toString.toUpperCase).append(' ')
    }

    if (capture.hasDefiniteSize && capture.nonEmpty) {
      if (sb.length > 7) sb.append("AND ")
      sb.append("( ").append(capture.mkString("f.capture_id = '", " OR f.capture_id = ','", "'")).append(") ")
    }
    if (sb.length > 7) {
      sb.toString()
    } else {
      " "
    }
  }
}
