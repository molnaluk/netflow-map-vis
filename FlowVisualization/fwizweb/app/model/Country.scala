package model

import anorm.SqlParser._
import anorm._
import play.api.Play.current
import play.api.db._

/**
 * Created by lukas on 26.4.15.
 */
case class Country(id: Int, code: String, name: String, continent: String)

object Country {

  val simple = {
    int("ID") ~ str("CODE") ~ str("NAME") ~ str("CONTINENT") map {
      case i ~ c ~ n ~ p => c -> Country(i, c, n, p)
    }
  }

  def aggregatorParser(aggreg: String) = {
    long(aggreg) ~ str("COUNTRY.CODE") map { case count ~ code => code.toLowerCase -> count }
  }

  def getAll(): Map[String, Country] = {
    DB.withConnection { implicit connection =>
      SQL("select * from Country").as(Country.simple *).toMap
    }
  }

}
