package model

import com.sanoma.cda.geo.Point

/**
 * Created by lukas on 8.5.15.
 */
case class GlobeViewModel(val countries: List[Seq[AggregatedFlow]],
                          val cities: Option[List[Seq[AggregatedFlow]]],
                          val continents: Option[List[Seq[AggregatedFlow]]])

case class GlobeViewModel2(val values: Map[String, Map[String, List[BigDecimal]]],
                           val coord: List[Option[Point]],
                           val cityNames: List[Option[String]],
                           val countries: Map[String, BigDecimal]
                            )