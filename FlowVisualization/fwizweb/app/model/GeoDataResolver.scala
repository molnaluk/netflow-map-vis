package model

import anorm._
import com.sanoma.cda.geoip.MaxMindIpGeo
import play.api.Play.current
import play.api.db.DB

import scala.collection.mutable.HashMap

/**
 * Created by lukas on 26.4.15.
 */
object GeoDataResolver {

  val geoIp = MaxMindIpGeo(current.configuration.getString("fwizweb.mmdb").get, 1000)
  val unknown = GeoData(0, GeoDataKey(0, Option("(unknown)"), None))
  val unknownCountry = Country(0, "", "(unknown)", "?")
  val geoTableByIP: HashMap[GeoDataKey, GeoData] = HashMap[GeoDataKey, GeoData]()
  val geoTable: HashMap[Long, GeoData] = HashMap[Long, GeoData]()
  val countries: Map[String, Country] = Country.getAll()
  val countries2: Map[Int, Country] = countries.values.map(c => c.id -> c).toMap
  //Country( row[Int]("id"),  row[String]("name"), Continent(row[Int]("continent")))) }


  def resolveIp(ip: String)(implicit conn: java.sql.Connection): GeoData = {
    val opt = geoIp.getLocation(ip)
    if (opt.isDefined) {
      val location = opt.get
      val gkey = GeoDataKey(getCountry(location.countryCode).id, location.city, location.geoPoint)
      val geo = geoTableByIP.getOrElse(gkey, unknown)
      if (geo.id == unknown.id) {
        val data = GeoData(geoTableByIP.size + 1, gkey)
        geoTableByIP.update(gkey, data)
        geoTable.update(data.id, data)
        GeoData.insertCache(data)
        data
      } else {
        geo
      }
    } else {
      unknown
    }
  }

  //  def IP2Geo(ip: String)(implicit conn: java.sql.Connection) = {
  //    val gd = geoTableByIP.getOrElse(ip, unknown)
  //    if (gd.id == unknown.id) {
  //      resolveIp(ip)
  //    } else {
  //      gd
  //    }
  //  }

  def getCountry(code: Option[String]) = if (code.isDefined) countries(code.get) else unknownCountry

  def getCountry(id: Int) = if (id > 0) countries2(id) else unknownCountry

  def reloadIPtable = DB.withConnection(implicit con => {
    geoTableByIP.clear()
    val newItems = SQL( """SELECT ip, geo
FROM (SELECT DISTINCT ip_src as ip, dst_geo as geo FROM Flow
UNION SELECT DISTINCT ip_dst as ip, dst_geo as geo FROM Flow); """)().map(r => {
      val id = r[Long]("geo")
      val geo = if (id == 0) unknown else geoTable(id)
    geo.key -> geo
    })//.toList
    geoTableByIP ++= newItems
    newItems
  })
}
