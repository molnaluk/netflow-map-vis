package model

import anorm._
import play.api.Play.current
import play.api.db.DB

/**
 * Created by lukas on 8.5.15.
 */
case class AggregatedFlow(
                           count: Long,
                           packet_count: BigDecimal,
                           data_size: BigDecimal,
                           duration: BigDecimal,
                           port: Option[Int] = None,
                           ip: Option[String] = None,
                           place: Option[String] = None,
                           geo: Option[GeoData] = None
                           )

object AggregatedFlows {

  def parseRow(r: Row, selector: String, hasGeo: Boolean) = {
    val count = r[Long]("aggregated_count")
    val geo = if (hasGeo) Some(GeoDataResolver.geoTable(r[Long]("GEODATA.GDID"))) else None
    val place = if (hasGeo) r[Option[String]](selector) else Some(r[String](selector))
    Seq(
      AggregatedFlow(count, r[Long]("packet_count_sum"), r[Long]("data_size_sum"), r[Long]("duration_sum"), place = place, geo = geo),
      AggregatedFlow(count, r[Long]("packet_count_avg"), r[Long]("data_size_avg"), r[Long]("duration_avg"), place = place)
    )
  }

  def find(selector: String, column: String, filter: FlowFilter, hasGeo: Boolean = false) = DB.withConnection { implicit connection => {
    val query = """SELECT
sum(f.packet_count) as packet_count_sum,
sum(f.data_size) as data_size_sum,
sum((f.end_time - f.start_time)) as duration_sum,
avg(f.packet_count) as packet_count_avg,
avg(f.data_size) as data_size_avg,
avg((f.end_time - f.start_time)) as duration_avg,
count(*) as aggregated_count,
                """ + selector + // src.country_id as cid, c.code, c.name, c.continent
      " FROM Flow  f " + filter.join() +
      filter.where() + "GROUP BY " + selector
    play.Logger.info(s"Aggreg find: ${selector}")
    //   play.Logger.info(query)
    SQL(query)().map(r => parseRow(r, column, hasGeo)).toList
  }
  }

  def turbo(li: List[Seq[AggregatedFlow]]) =
    Seq("count" -> li.map(s => BigDecimal(s(0).count)),
      "data_size_sum" -> li.map(s => s(0).data_size),
      "packet_count_sum" -> li.map(s => s(0).packet_count),
      "duration_sum" -> li.map(s => s(0).duration),
      "data_size_avg" -> li.map(s => s(1).data_size),
      "packet_count_avg" -> li.map(s => s(1).packet_count),
      "duration_avg" -> li.map(s => s(1).duration)
    ).toMap

  def countryTurbo(li: Seq[AggregatedFlow]): Map[String, BigDecimal] =
    Seq("count" -> BigDecimal(li(0).count),
      "data_size_sum" -> li(0).data_size,
      "packet_count_sum" -> li(0).packet_count,
      "duration_sum" -> li(0).duration,
      "data_size_avg" -> li(1).data_size,
      "packet_count_avg" -> li(1).packet_count,
      "duration_avg" -> li(1).duration
    ).toMap


  def coord(li: List[Seq[AggregatedFlow]]) =
    li.map(s => if (s(0).geo.isDefined) s(0).geo.get.key.point else None)
}
