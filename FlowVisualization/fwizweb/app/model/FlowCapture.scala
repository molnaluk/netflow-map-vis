package model

import anorm._
import play.api.Play.current
import play.api.db.DB

/**
 * Created by lukas on 26.4.15.
 */
case class FlowCapture(id: Int, start_time: Long, end_time: Long, path: String, system: String, description: Option[String])

object FlowCapture {

  def create(fc: FlowCapture) = {
    val pk = DB.withConnection { conn =>
      SQL(
        """INSERT INTO FlowCapture
          (START_TIME, END_TIME, PATH, SYSTEM, DESCRIPTION)
          VALUES ({start_time}, {end_time}, {path}, {system}, {description})"""
      ).on('start_time -> fc.start_time, 'end_time -> fc.end_time,
          'path -> fc.path, 'system -> fc.system, 'description -> fc.description
        ).executeInsert()(conn)
      // SQL("SELECT max(id) FROM FlowCapture").as(scalar[Int].single)(conn)
    } match {
      case Some(id) => id.asInstanceOf[Int]
    }
    FlowCapture(pk, fc.start_time, fc.end_time, fc.path, fc.system, fc.description)
  }

  def update(id: Int)(implicit con: java.sql.Connection) = {
    SQL(
      """UPDATE flowcapture
SET end_time = (select max(end_time) from flow where  capture_id = {id}),
start_time = (select min(start_time) from flow where  capture_id = {id})
      """
    ).on('id -> id).executeUpdate()
  }

}
