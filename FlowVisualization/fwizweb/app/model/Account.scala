package model

// import org.mindrot.jbcrypt.BCrypt_

import anorm.SqlParser._
import anorm._
import play.api.Play.current
import play.api.db._

case class Account(id: Int, email: String, password: String, name: String, role: Role)

object Account {

  val simple = {
    int("AID") ~ str("EMAIL") ~ str("PASSWORD") ~ str("NAME") ~ str("ROLE") map {
      case i ~ e ~ p ~ n ~ r => Option(Account(i, e, p, n, Role.valueOf(r)))
      case _ => None
    }
  }

  def authenticate(email: String, password: String): Option[Account] = {
    // Logger.info(s"authenticate ${email}, ${password}")
    findByEmail(email).filter { a => password == a.password
      // BCrypt.checkpw(password, a.password)
    }
  }

  def findByEmail(email: String) = DB.withConnection { implicit connection =>
    SQL("select * from Account WHERE email = {email};").on("email" -> email).as(Account.simple.single)
  }

  def findById(id: Int) = DB.withConnection { implicit connection =>
    SQL("select * from Account WHERE aid = {id};").on("id" -> id).as(Account.simple.single)
  }

  def findAll() = DB.withConnection { implicit connection =>
    SQL("select * from Account;").as(Account.simple *)
  }

  def create(a: Account) = DB.withConnection { implicit connection =>
    val pass = a.password //  BCrypt.hashpw(a.password, BCrypt.gensalt())
    SQL("INSERT INTO Account (email, password, name, role) VALUES ({email}, {password}, {name}, {role});").on(
      "email" -> a.email, "password" -> pass, "name" -> a.name, "role" -> a.role.toString()).executeInsert()
  }

}
