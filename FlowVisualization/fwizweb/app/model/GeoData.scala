package model

import anorm.SqlParser._
import anorm._
import com.sanoma.cda.geo.Point
import play.api.Play.current
import play.api.db.DB

import scala.collection.mutable

/**
 * Created by lukas on 26.4.15.
 */
//case class GeoData(id: Long, country_id: Int, city: Option[String], point: Option[Point])

case class GeoData(id: Long, key: GeoDataKey)

case class GeoDataKey(country_id: Int, city: Option[String], point: Option[Point])

object GeoData {
  val cache = mutable.MutableList[GeoData]()

  val simple = {
    long("GDID") ~ int("COUNTRY_ID") ~ str("CITY") ~ double("lat") ~ double("lng") map {
      //case i ~ c ~ n ~ None ~ _ => GeoData(i, c, Option(n), None)
      case i ~ c ~ n ~ la ~ lo => if (la == None || lo == None) GeoData(i, GeoDataKey(c, Option(n), None)) else GeoData(i, GeoDataKey(c, Option(n), Option(Point(la, lo))))
    }
  }

  def insertCache(gd: GeoData)(implicit con: java.sql.Connection) = {
    cache += gd
    if (cache.size >= 1000) {
      flushCache
    }
  }

  def flushCache()(implicit con: java.sql.Connection) = {
    if (cache.isEmpty == false) {
      insertBatch(cache)
      cache.clear()
    }
  }

  def insert(gd: GeoData)(implicit con: java.sql.Connection) = {
    val lat = if (gd.key.point.isDefined) Some(gd.key.point.get.latitude) else None
    val lng = if (gd.key.point.isDefined) Some(gd.key.point.get.longitude) else None
    SQL(s"INSERT INTO GeoData VALUES ({id}, {country_id}, {city}, {lat}, {lng});").on(
      "id" -> gd.id, "country_id" -> gd.key.country_id, "city" -> gd.key.city, "lat" -> lat, "lng" -> lng
    ).executeInsert()
    gd
  }

  def insertBatch(cache: TraversableOnce[GeoData])(implicit con: java.sql.Connection) = {
    BatchSql( SQL(s"INSERT INTO GeoData VALUES ({id}, {country_id}, {city}, {lat}, {lng});"),
    cache.map( gd => {
      val lat = if (gd.key.point.isDefined) Some(gd.key.point.get.latitude) else None
      val lng = if (gd.key.point.isDefined) Some(gd.key.point.get.longitude) else None
      Seq[NamedParameter]("id" -> gd.id, "country_id" -> gd.key.country_id, "city" -> gd.key.city, "lat" -> lat, "lng" -> lng)
    }
    ).toSeq).execute()
  }

  def findAll = DB.withConnection { implicit con =>
    SQL("SELECT * FROM GeoData;").apply().collect({
      case Row(gdid: Long, country_id: Int, city: Option[String], Some(latitude: Double), Some(longitude: Double)) => GeoData(gdid, GeoDataKey(country_id, city, Option(Point(latitude, longitude))))
      case Row(gdid: Long, country_id: Int, city: Option[String], _, _) => GeoData(gdid, GeoDataKey(country_id, city, None))
      //case _ => None
    }).toList
  }
}
