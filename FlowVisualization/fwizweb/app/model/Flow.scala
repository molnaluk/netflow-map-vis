package model

import anorm.SqlParser._
import anorm._
import parsers.FlowCapturer
import play.api.Play._
import play.api.db.DB

import scala.collection.mutable

/**
 * Created by lukas on 22.4.15.
 */
case class Flow(
                 //id: Int
                 capture_id: Int
                 , ip_src: String
                 , ip_dst: String
                 , port_src: Int
                 , port_dst: Int
                 , start_time: Long
                 , end_time: Long
                 , packet_count: Long
                 , data_size: Long
                 , protocol: Int
                 , src_geo: Option[GeoData] = None
                 , dst_geo: Option[GeoData] = None
                 , ipv6: Boolean = false
                 )


object Flows {
  def simple(geo: mutable.HashMap[Long, GeoData]) = {
    int("capture_id") ~ str("ip_src") ~ str("ip_dst") ~ int("port_src") ~
      int("port_dst") ~ long("start_time") ~ long("end_time") ~ long("packet_count") ~ long("data_size") ~
      int("protocol") ~ get[Option[Long]]("src_geo") ~ get[Option[Long]]("dst_geo") ~ bool("ipv6") map {
      case c ~ ips ~ ipd ~ ps ~ pd ~ st ~ et ~ pc ~ ds ~ p ~ sg ~ dg ~ i6 => {
        val sg1 = if (sg.isDefined && sg.get > 0) Option(geo(sg.get)) else None
        val dg1 = if (dg.isDefined && dg.get > 0) Option(geo(dg.get)) else None
        Flow(c, ips, ipd, ps, pd, st, et, pc, ds, p, sg1, dg1, i6)
      }
    }
  }

  def count = DB.withConnection { implicit con =>
    anorm.SQL("SELECT count(*) FROM Flow;").as(SqlParser.scalar[Long].single)
  }

  def globe(filter: FlowFilter) = {

  }

  def country(filter: FlowFilter) = DB.withConnection { implicit connection => {
    val query = """SELECT
count(src.country_id) as src_count,
sum(f.packet_count) as packet_count_sum,
sum(f.data_size) as data_size_sum,
sum((f.end_time - f.start_time)) as duration_sum,
avg(f.packet_count) as packet_count_avg,
avg(f.data_size) as data_size_avg,
avg((f.end_time - f.start_time)) as duration_avg,
src.country_id as cid, c.code, c.name, c.continent
        FROM Flow  f
      INNER JOIN GeoData src ON f.src_geo = src.gdid
      INNER JOIN Country c ON c.id = src.country_id""" +
      filter.where() + "GROUP BY src.country_id, c.name, c.continent;"
    play.Logger.info(query)
    SQL(query).as(Country.aggregatorParser("SRC_COUNT") *).toMap
  }
  }


  def find(id: Long) = DB.withConnection { implicit connection =>
    SQL("select * from Flow WHERE id = {fid};").on("fid" -> id).as(Flows.simple(GeoDataResolver.geoTable).single)
  }

  def all() = DB.withConnection { implicit connection =>
    SQL("select * from Flow;").as(Flows.simple(GeoDataResolver.geoTable) *)
  }

  def load(path: Option[String], capturer: FlowCapturer) = {
    val pcapFile = if (path.isDefined) path.get else current.configuration.getString("fwizweb.default.pcap").get
    val sqlInsert = SQL( """INSERT INTO Flow
(CAPTURE_ID, IP_SRC, IP_DST, PORT_SRC, PORT_DST, START_TIME, END_TIME,
PACKET_COUNT, DATA_SIZE, SRC_GEO, DST_GEO, IPV6, PROTOCOL)
VALUES
({capture_id}, {ip_src}, {ip_dst}, {port_src}, {port_dst},
{start_time}, {end_time}, {packet_count}, {data_size},
{src_geo}, {dst_geo}, {ipv6}, {protocol});""")

    DB.withConnection { implicit con => {
      val capt = capturer.readFile(pcapFile, (fsq, cid) => {
        BatchSql(sqlInsert, fsq.map(fi => {
          val f = fi.toFlow(cid)
          Seq[NamedParameter]("capture_id" -> f.capture_id,
            "ip_src" -> f.ip_src,
            "ip_dst" -> f.ip_dst,
            "port_src" -> f.port_src,
            "port_dst" -> f.port_dst,
            "start_time" -> f.start_time,
            "end_time" -> f.end_time,
            "packet_count" -> f.packet_count,
            "data_size" -> f.data_size,
            "src_geo" -> f.src_geo.get.id,
            "dst_geo" -> f.dst_geo.get.id,
            "ipv6" -> f.ipv6,
            "protocol" -> f.protocol)
        })).execute()
        GeoData.flushCache()
        true
      })
      if (capt.isDefined) FlowCapture.update(capt.get.id)
      capt
    }
    }
  }
}