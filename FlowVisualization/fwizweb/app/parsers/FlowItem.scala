package parsers

import model.{Flow, GeoDataResolver}

/**
 * Created by lukas on 19.4.15.
 */
trait FlowItem {

  //def toFlow(capture_id: Int)(implicit con : java.sql.Connection): Flow

  def endTime: Long

  def startTime: Long

  def octetCount: Long

  def packetCount: Long

  def inputInterface: Int

  def outputInterface: Int

  def srcAddr: String

  def dstAddr: String

  def protocol: Int

  def srcPort: Int

  def dstPort: Int

  def nextHop: String

  def IPv6: Boolean

  def toFlow(capture_id: Int)(implicit con: java.sql.Connection): Flow = {
    val src_ip = srcAddr
    val dst_ip = dstAddr
    Flow(capture_id,
      src_ip,
      dst_ip,
      srcPort,
      dstPort,
      startTime,
      endTime,
      packetCount,
      octetCount,
      protocol,
      Option(GeoDataResolver.resolveIp(src_ip)),
      Option(GeoDataResolver.resolveIp(dst_ip)), IPv6)

  }
}
