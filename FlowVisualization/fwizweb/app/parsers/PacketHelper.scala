package parsers

/**
 * Created by lukas on 19.4.15.
 */
case class PacketHelper(data: Array[Byte], start: Int) {

  def getIp(offset: Int) = "%d.%d.%d.%d".format(getUByte(offset), getUByte(offset + 1),
    getUByte(offset + 2), getUByte(offset + 3))

  def getIpV6(offset: Int) = Range(offset, offset + 16, 2).map(o => getUShort(o).formatted("%x")).mkString(sep = ":")

  def get(offset: Int) = data.apply(start + offset)

  def getUByte(offset: Int) = 0x000000FF & get(offset)

  def getUShort(offset: Int) = ((0x000000FF & get(offset)) << 8) | (0x000000FF & get(offset + 1))

  def getUInt(offset: Int) = {
    (((0x000000FF & get(offset)) << 24) |
      ((0x000000FF & get(offset + 1)) << 16) |
      ((0x000000FF & get(offset + 2)) << 8) |
      (0x000000FF & get(offset + 3))) & 0xFFFFFFFFL
  }

  def next(offset: Int) = PacketHelper(data, start + offset)
}
