package parsers

import model.FlowCapture

/**
 * Created by lukas on 3.5.15.
 */
trait FlowCapturer {

  //val flows = new mutable.MutableList[Flow]()
  def readFile(path: String, itemFunc: (Seq[FlowItem], Int) => Boolean): Option[FlowCapture]

  // def clear()
}
