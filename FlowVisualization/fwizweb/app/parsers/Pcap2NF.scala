package parsers

import java.io.EOFException

import model.FlowCapture
import org.pcap4j.core.{PcapHandle, Pcaps}

import scala.collection.mutable

class Pcap2NF(val flowFilter: (FlowItem) => Boolean, val limit: Option[Int] = None) extends FlowCapturer {

  def readFile(path: String, flowFunc: (Seq[FlowItem], Int) => Boolean): Option[FlowCapture] = {
    val capt = FlowCapture.create(FlowCapture(0, 0, 0, path, "CESNET2", None))
    readPackets(Pcaps.openOffline(path), capt.id, flowFunc).close()
    Some(capt)
  }

  protected def readPackets(handle: PcapHandle, capture_id: Int, flowFunc: (Seq[FlowItem], Int) => Boolean): PcapHandle = {
    val batch = mutable.MutableList[FlowItem]()
    for (a <- Range(0, if (limit.isDefined) limit.get else Int.MaxValue)) {
      try {
        batch ++= readPacket(handle)
        if (batch.length > 1000) {
          if (false == flowFunc(batch.toSeq, capture_id)) {
            return handle
          }
          batch.clear()
        }
      } catch {
        case eof: EOFException =>
          println(s"EOF: ${eof}")
          return handle
        case t: Throwable => println(s"something else: ${t}")
      }
    }
    handle
  }

  def readFlowSet(nfp: NetFlowPacket, helper: PacketHelper) = {
    if (helper.start < nfp.length) {
      val fset = FlowSet(helper) // TODO universalize
      //flowsNF ++=
      (fset.flowSetID match {
        case 257 | 256 => fset.flows(h => new NetFlowItemIPV4(h))
        case 258 => fset.flows(h => new NetFlowItemIPV6(h))
        case 0 => List()
      }) filter flowFilter
    } else {
      List()
    }
  }

  def readPacket(handle: PcapHandle) = {
    val packet = handle.getNextPacketEx()
    //  println(packet)
    val helper = PacketHelper(packet.getRawData(), 42)
    val nfp = new NetFlowPacket(helper, helper.data.length)
    // flowPackets += nfp
    readFlowSet(nfp, helper.next(nfp.HeaderSize))
  }
}
