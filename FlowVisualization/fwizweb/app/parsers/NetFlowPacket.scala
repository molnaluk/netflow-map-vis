package parsers


/**
 * Created by lukas on 7.5.15.
 */
case class NetFlowPacket(val packet: PacketHelper, len: Int) {
  final val HeaderSize = 20

  def version = packet.getUByte(0)

  def flowCount = packet.getUByte(2)

  def sysUptime = packet.getUInt(4)

  def timestamp = packet.getUInt(8)

  def flowSeq = packet.getUInt(12)

  def sourceId = packet.getUInt(16)

  def length = len

}

case class FlowSetTemplate(val packet: PacketHelper) {
  def flowSetID = packet.getUShort(0)

  def fieldCount = packet.getUShort(2)

  def length = 4 + (2 * fieldCount)
}


case class FlowSet(val packet: PacketHelper) {
  def flowSetID = packet.getUShort(0)

  def length = packet.getUShort(2)

  def fieldCount = if (flowSetID == 258) 18 else 21

  def flows(ctor: PacketHelper => FlowItem) = {
    val itemLength = flowSetID match {
      case 257 => 48
      case 256 => 52
      case 258 => 72
    }
    //Logger.info(Range(4, length - itemLength, itemLength).toString())
    //Logger.info(s"itemLenght: ${itemLength}, length:${length}, fieldCount:${fieldCount}, Range(4, ${length-1}, ${itemLength}) ")
    Range(4, length - itemLength, itemLength).map(i => ctor(packet.next(i)))
  }
}
