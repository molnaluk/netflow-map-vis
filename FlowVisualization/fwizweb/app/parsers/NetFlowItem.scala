package parsers

/**
 * Created by lukas on 18.4.15.
 */
class NetFlowItemIPV6(val packet: PacketHelper) extends FlowItem {

  //def IPv6 = false

  def endTime = packet.getUInt(0)

  def startTime = packet.getUInt(4)

  def octetCount = packet.getUInt(8)

  def packetCount = packet.getUInt(12)

  def inputInterface = packet.getUShort(16)

  def outputInterface = packet.getUShort(18)

  def srcAddr = packet.getIpV6(20)

  def dstAddr = packet.getIp(36)

  def protocol = packet.getUByte(50)

  def IPTos = packet.getUByte(51)

  def srcPort = packet.getUShort(52)

  def dstPort = packet.getUShort(54)

  def samplerID = None

  def flowClass = None // packet.getUByte(35)

  def nextHop = packet.getIpV6(56)

  def dstMask = packet.getUByte(70)

  def srcMask = packet.getUByte(71)

  def TCPFlags = packet.getUByte(72)

  def dstAS = packet.getUShort(73)

  def srcAS = packet.getUShort(75)

  def IPv6 = true
}

class NetFlowItemIPV4(val packet: PacketHelper) extends FlowItem {

  //def IPv6 = false

  def endTime = packet.getUInt(0)

  def startTime = packet.getUInt(4)

  def octetCount = packet.getUInt(8)

  def packetCount = packet.getUInt(12)

  def inputInterface = packet.getUShort(16)

  def outputInterface = packet.getUShort(18)

  def srcAddr = packet.getIp(20)

  def dstAddr = packet.getIp(24)

  def protocol = packet.getUByte(28)

  def IPTos = packet.getUByte(29)

  def srcPort = packet.getUShort(30)

  def dstPort = packet.getUShort(32)

  def samplerID = packet.getUByte(34)

  def flowClass = packet.getUByte(35)

  def nextHop = packet.getIp(36)

  def dstMask = packet.getUByte(40)

  def srcMask = packet.getUByte(41)

  def TCPFlags = packet.getUByte(42)

  def direction = packet.getUByte(43)

  def IPv6 = false
}
