# Flows SCHEMA

# --- !Ups

CREATE TABLE GeoData (
  gdid       BIGINT        NOT NULL AUTO_INCREMENT,
  country_id INT           NOT NULL,
  city       VARCHAR2(255) NULL,
  latitude   DOUBLE        NULL,
  longitude  DOUBLE        NULL,
  PRIMARY KEY (gdid)
);


# --- !Downs

DROP TABLE GeoData;