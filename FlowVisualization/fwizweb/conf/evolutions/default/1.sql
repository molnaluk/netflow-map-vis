# Flows SCHEMA

# --- !Ups

CREATE TABLE Flow (
  fid          BIGINT      NOT NULL AUTO_INCREMENT,
  capture_id   INT         NOT NULL,
  ip_src       VARCHAR(39) NOT NULL,
  ip_dst       VARCHAR(39) NOT NULL,
  port_src     INTEGER     NOT NULL,
  port_dst     INTEGER     NOT NULL,
  start_time   BIGINT      NOT NULL,
  end_time     BIGINT      NOT NULL,
  packet_count BIGINT      NOT NULL,
  data_size    BIGINT      NOT NULL,
  src_geo      BIGINT      NOT NULL,
  dst_geo      BIGINT      NOT NULL,
  ipv6         BIT         NOT NULL,
  protocol     INT         NOT NULL,
  PRIMARY KEY (fid)
);

# --- !Downs

DROP TABLE Flow;