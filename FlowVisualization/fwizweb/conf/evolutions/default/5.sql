# Account SCHEMA

# --- !Ups

CREATE TABLE Account (
  aid      INT           NOT NULL AUTO_INCREMENT,
  email    VARCHAR2(100) NOT NULL UNIQUE,
  password VARCHAR2(100) NOT NULL,
  name     VARCHAR2(50)  NOT NULL,
  role     VARCHAR(16)   NOT NULL,
  PRIMARY KEY (aid)
);

# --- !Downs

DROP TABLE Account;


