# Flows SCHEMA

# --- !Ups

CREATE TABLE FlowCapture (
  id          INT            NOT NULL AUTO_INCREMENT,
  start_time  BIGINT         NOT NULL,
  end_time    BIGINT         NOT NULL,
  path        VARCHAR2(32)   NOT NULL,
  system      VARCHAR2(32)   NOT NULL,
  description VARCHAR2(1024) NULL,
  PRIMARY KEY (id)
);


# --- !Downs

DROP TABLE FlowCapture;